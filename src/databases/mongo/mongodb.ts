import { Database } from '../database_abstract';
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

import { FlightsModel } from './models/flights.model';
import { loadData } from '../../../scripts/base-data';
import { PersonsModel } from './models/persons.model';

export class MongoStrategy extends Database {
    constructor() {
        super();
        this.getInstance();
    }

    private async getInstance() {
        const mongo = await MongoMemoryServer.create();
        const uri = mongo.getUri();

        const mongooseOpts = {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        };

        const flights = [
            {
                code: 'ABC-123',
                origin: 'EZE',
                destination: 'LDN',
                status: 'ACTIVE',
            },
            {
                code: 'XYZ-678',
                origin: 'CRC',
                destination: 'MIA',
                status: 'ACTIVE',
            },
        ];

        (async () => {
            await mongoose.connect(uri, mongooseOpts);
            // await FlightsModel.create(flights);
            await loadData();
        })();
    }

    public addFlight(flight: { code: string; origin: string; destination: string; passengers: string[]; status: string; }) {
        return FlightsModel.create(flight)
    }

    public async getFlights() {
        return FlightsModel.find({});
    }

    public async getPersons() {
        return PersonsModel.find({});
        
    }

    public addPerson(person: { name: string; gender: string; email: string; }) {
        return PersonsModel.create(person);   
    }
}
